# 基于Open CV的植物图像分类识别项目

#### Description
项目主要是基于Open CV进行植物图像进行分类识别。展示部分采用了网页的形式（Vue+Element+.net Core），由用户上传图片，服务器返回该图片的分类结果。Web服务（.net Core）和c++图像处理模块的交互采用了TCP的形式，即利用.net的TCP客户端和Qt的Tcp服务器端进行交互。最后是Qt的服务器端调用了c++的图像处理类，返回分类识别结果。有一点需要注意，进行图像处理前，先进行图像训练。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
