# 基于Open CV的植物图像分类识别项目

#### 介绍
项目主要是基于Open CV进行植物图像进行分类识别。展示部分采用了网页的形式（Vue+Element+.net Core），由用户上传图片，服务器返回该图片的分类结果。Web服务（.net Core）和c++图像处理模块的交互采用了TCP的形式，即利用.net的TCP客户端和Qt的Tcp服务器端进行交互。最后是Qt的服务器端调用了c++的图像处理类，返回分类识别结果。有一点需要注意，进行图像处理前，先进行图像训练。如有疑问可通过博客评论联系我，主要内容详见我的博客https://blog.csdn.net/qq_37105120/article/details/106390041

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/224639_0976b72c_7488979.png "系统总体结构图2.png")


#### 安装教程

1.  Vue安装配置过程，可以直接看https://www.bilibili.com/video/BV1EE411B7SU?p=18前几节内容。
2.  .Net Core安装配置过程，可以看https://www.bilibili.com/video/BV11E411n74a?from=search&seid=16803251335123231391前半段视频。
3.  VS2019配置Qt，配置过程详见https://www.jianshu.com/p/1db7fbe407f8
4.  OpenCV配置过程，配置的详见https://blog.csdn.net/xinjiang666/article/details/80785210

#### 使用说明

1.  图像处理类的Categorizer.h文件中包含了两个重要的变量，const int clusters（集群计数）,const int categories_size（植物的类型数目）。需要根据不同的情况自己去修改。ini_picture()这个函数非常重要，主要是读取图像，还有给category_name[i]赋值。这部分要自己去写，或者用我的，但是目录要正确，我已经在相关的资源文件中写了备注。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
